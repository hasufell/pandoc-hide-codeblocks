# pandoc-hide-codeblocks

This is a [`pandoc`](https://pandoc.org) filter
that hides code blocks marked as `.hidden`
and warns about code blocks that
are neither marked as `.haskell`,
nor [`.dot`](https://graphviz.org).
